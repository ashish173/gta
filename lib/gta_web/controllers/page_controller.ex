defmodule GtaWeb.PageController do
  use GtaWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
